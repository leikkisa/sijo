# Sijo

This is going to be the future home of Sijo, a Korean-inspired clothing brand.  The overall design and content has been provided and driven by the client.

## Demo
[https://sijo.surge.sh/](https://sijo.surge.sh/ "Sijo")

## Screenshot
![](screenshot.png)

## Getting Started

To view the website, clone the repo and open the index.html file.  That's it!

The project is built in Node.js, and the CSS is built using Sass.

To install:

```$ npm install```

To compile Sass:

```$ npm run compile:sass```

## Built with
- HTML
- Sass

## Acknowledgements
- Some photos are from [https://unsplash.com/](https://unsplash.com/ "Unsplash")
- Video from [http://coverr.co/](http://coverr.co/ "Coverr")
